### 介绍

此仓库为奇特物联(iotkit)物联网平台的前端项目。

系统包含了品类、物模型、消息转换、通讯组件（mqtt 通讯组件、小度音箱接入组件、onenet Studio 接入组件）、modbus透传接入、modbus虚拟网关、云端低代码设备开发、设备管理、规则引擎、第三方平台接入、数据流转、数据可视化、报警中心等模块和智能家居 APP（小程序）。

**前端项目见：** https://gitee.com/iotkit-open-source/iot-console-web

**演示地址：** [演示地址](http://159.75.222.119:16808)，账号：guest1,密码：guest123 (只读权限)

**智能家居小程序：** https://gitee.com/iotkit-open-source/iot-mp-home

### 技术栈

使用的框架和组件包含 vue2.0、element-ui、codemirror、echarts 等，集成了 Sa-Token 的 OAuth2 认证。

### 文档
http://iotkit-open-source.gitee.io/document/


### 前端配置：

默认不需要更改

.env.development

```
#填写后台接口地址
VUE_APP_API_URL=http://127.0.0.1:8086
#填写后台接口路径
VUE_APP_API_BASE_PATH=/webapi
#填写认证中心地址
VUE_APP_AUTH_URL=http://127.0.0.1:8086/oauth2
#填写oauthClient表中的clientId
VUE_APP_AUTH_CLIENTID=iotkit
```

### 安装：

参考运行环境：

npm：6.14.13，node：v14.17.0


npm install --save

### 启动：

npm run serve

### 打包：

npm run build


### 服务器部署配置

修改.env.production文件中的ip为服务器外网IP


nginx配置代理:

```
server {
    listen       80;
    server_name  localhost;

    location /oauth2/ {
        proxy_pass http://127.0.0.1:8086/oauth2/;
    }

    location /webapi/ {
        proxy_pass http://127.0.0.1:8086/;
    }

    location / {
        root /前端包目录/dist;
        index  index.html index.htm;
        try_files $uri $uri/ /index.html;
    }
}
```



### 界面截图

![输入图片说明](doc/images/WechatIMG538.png)
![输入图片说明](doc/images/WechatIMG539.png)
![输入图片说明](doc/images/1.png)
![输入图片说明](doc/images/2.png)
![输入图片说明](doc/images/3.png)
![输入图片说明](doc/images/4.png)
![输入图片说明](doc/images/5.png)
![输入图片说明](doc/images/6.png)
![输入图片说明](doc/images/7.png)
![输入图片说明](doc/images/8.png)
![输入图片说明](doc/images/8_2.png)
![输入图片说明](doc/images/WX20220530-095227@2x.png)
![输入图片说明](doc/images/9.png)
![输入图片说明](doc/images/9_2.png)
![输入图片说明](doc/images/9_3.png)
![输入图片说明](doc/images/9_4.png)
![输入图片说明](doc/images/10.png)
![输入图片说明](doc/images/11.png)
![输入图片说明](doc/images/11_2.png)
![输入图片说明](doc/images/11_3.png)
![输入图片说明](doc/images/12.png)
![输入图片说明](doc/images/13.png)
![输入图片说明](doc/images/14.png)
![输入图片说明](doc/images/15.png)
![输入图片说明](doc/images/15_1.png)
![输入图片说明](doc/images/16.png)
![输入图片说明](doc/images/16_1.png)
![输入图片说明](doc/images/16_2.png)
![输入图片说明](doc/images/18.png)
![输入图片说明](doc/images/17.png)


#### 捐助与支持
如果您觉得我的开源软件对你有所帮助请关注、star、fork :kissing_heart: 。

如果我的开源软件应用到您的商业项目中，请务必通知到我，因为得到用户的认可是支撑开源的动力。

交流QQ群: 940575749

微信群(添加我微信备注"进群"):

![输入图片说明](https://gitee.com/iotkit-open-source/iotkit-parent/raw/master/doc/ma.png) 
